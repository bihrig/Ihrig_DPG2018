@title[Title]

### Quantum critical behavior of Dirac systems at higher-loop order


*Bernhard Ihrig*<sup>1</sup>, Luminita Mihaila<sup>2</sup>, Nikolai Zerf<sup>3</sup> and Michael M. Scherer<sup>1</sup>

<sup>1</sup> University of Cologne, <sup>2</sup> University of Heidelberg, <sup>3</sup> Humboldt-University Berlin

<sub><sup>[arXiv:1703.08801](https://arxiv.org/abs/1703.08801) & [arXiv:1709.05057](https://arxiv.org/abs/1709.05057)</sup></sub>

---

@title[Dirac Systems]

* Dirac systems got a lot of attention in the last years |
* The show critical behavior leading to new |

### fermionic universality classes

* Many complementary methods to study on these |
  * Monte Carlo `\(\rightarrow\)` now sign-problem free! |
  * Renormalization Group `\(\rightarrow\)` (non)-perturbative approach ? |
  * conformal bootstrap `\(\rightarrow\)` forget about the Hamiltonian! |

---

### Why should we be interested ?

*Remember:* critical behavior is characterized by critical exponents of universal singular quantities
$$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$


---?code=assets/md/hello.md&title=Step 1. PITCHME.md

<br>
#### Create slideshow content using GitHub Flavored Markdown in your favorite editor.

<span class="aside">It's as easy as README.md with simple slide-delimeters (---)</span>

---

@title[Step 2. Git-Commit]

### <span class="gold">STEP 2. GIT-COMMIT</span>
<br>

```shell
$ git add PITCHME.md
$ git commit -m "New slideshow content."
$ git push

Done!
```

@[1](Add your PITCHME.md slideshow content file.)
@[2](Commit PITCHME.md to your local repo.)
@[3](Push PITCHME.md to your public repo and you're done!)
@[5](Supports GitHub, GitLab, Bitbucket, GitBucket, Gitea, and Gogs.)

---

@title[Step 3. Done!]

### <span class="gold">STEP 3. GET THE WORD OUT!</span>
<br>
![GitPitch Slideshow URLs](assets/images/gp-slideshow-urls.png)
<br>
<br>
#### Instantly use your GitPitch slideshow URL to promote, pitch or present absolutely anything.

---

@title[Slide Rich]

### <span class="gold">Slide Rich</span>

#### Code Presenting for Blocks, Files, and GISTs
#### Image, Video, Chart, and Math Slides
#### Multiple Themes with Easy Customization
<br>
#### <span class="gold">Plus collaboration is built-in...</span>
#### Your Slideshow is Part of Your Project
#### Under Git Version Control within Your Git Repo

---

@title[Feature Rich]

### <span class="gold">Feature Rich</span>

#### Present Online or Offline
#### With Speaker Notes Support
#### Print Presentation as PDF
#### Auto-Generated Table-of-Contents
#### Share Presentation on Twitter or LinkedIn

---

### <span class="gold">GitPitch Pro - Now Live!</span>

<br>
<div class="left">
    <i class="fa fa-user-secret fa-5x" aria-hidden="true"> </i><br>
    <a href="https://gitpitch.com/pro-features" class="pro-link">
    More details here.</a>
</div>
<div class="right">
    <ul>
        <li>Private Repos</li>
        <li>Private URLs</li>
        <li>Password-Protection</li>
        <li>Image Opacity</li>
        <li>SVG Image Support</li>
    </ul>
</div>

---

### Go for it.
### Just add <span class="gold">PITCHME.md</span> ;)
<br>
[Click here to learn more @fa[external-link fa-pad-left]](https://github.com/gitpitch/gitpitch/wiki)
